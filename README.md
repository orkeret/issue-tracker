#Issue tracker Spring-based project

Main tech stack:
Java 8, Spring boot, Spring Data JPA, MySQL & Spring WEB.

##Getting started  
Currently the application assumes that you have a locally running MySQL server (localhost:3306) and that the app can create a new DB with the name "issue_tracker".  
You could change the location of the MySQL server that you wish to connect to and the DB name via application.properties.  
The application is hosted (with the current configuration) on localhost:8080.  


##Rest Endpoints  
GET http://localhost:8080/rest/users - retreiving all the users  
POST http://localhost:8080/rest/users - creating a new user  
GET http://localhost:8080/rest/users/{id} - retreiving a user with the given ID  
PUT http://localhost:8080/rest/users/{id} - updating the user with the given ID (ETag and If-Match are in-use in order to avoid "Lost Update" Problem)  
DELETE http://localhost:8080/rest/users/{id} - deleting the user with the given ID  

GET http://localhost:8080/rest/issues - retreiving all the issues  
POST http://localhost:8080/rest/issues?transitionId={transitionId} - creating a new issue  
GET http://localhost:8080/rest/issues/{id} - retreiving an issue with the given ID  
PUT http://localhost:8080/rest/issues/{id} - updating the issue with the given ID (ETag and If-Match are in-use in order to avoid "Lost Update" Problem)  
DELETE http://localhost:8080/rest/issues/{id} - deleting the issue with the given ID  
POST http://localhost:8080/rest/issues/{id}/transit?transitionId={transitionId} - issueing a transition which causes a state change  

* transitionId should be supplied when creating an issue since there could be multiple available initial transitions (Worfklow supports it). we could also fix it to "CREATE ISSUE" and make it a non-deletable transition.  
* Please refer to classes that are annotated with @RestController in order to see the full REST API.

##Important notes  
1) GET/DELETE .../{id} request to a non-existing ID would cause a 404 response.  
2) When trying to create or update an issue - you must supply an existing assigneeId (user.id of an existing user).  
3) DELETE http://localhost:8080/rest/users/{id} would fail if you would try to delete a user that has resources assign to it (currently just issues).  
4) PUT http://localhost:8080/rest/issues/{id} or PUT http://localhost:8080/rest/users/{id} would only serve update** requests for existing resources.  
Requests should have an If-Match header with the appropriate value in order to update an existing resource.  
"Optimistic lock" is more scalable than "Pessimistic locking" when dealing with a highly concurrent environment.  
However, pessimistic locking is a better solution for situations where the possibility of simultaneous updates to the same data by multiple sources is common.  
For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates to the same resource.  
If proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:  
Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)  
Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)  
** PUT .../{id} for a non-existing resource would cause a 400 response.  
I've decided to go with that approach for now as I haven't found a non-hacky and robust way to achieve "Use manually provided ID or generate one".  
I did found a few hacky solutions that led to some race-conditions.  
Anyway, there isn't any reason that a client would decide the ID for creation of a resource when it could just use a POST request therefore I'm happy with this solution.  
Future enhancement - maintain an in-memory model/cache as it would be beneficial for the read OPs.  