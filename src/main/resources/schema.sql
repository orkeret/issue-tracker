CREATE TABLE IF NOT EXISTS state(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	version BIGINT(20) NOT NULL,
	name VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS transition(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	version BIGINT(20) NOT NULL,
	name VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	version BIGINT(20) NOT NULL,
	email VARCHAR(255),
	name VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS issue(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	version BIGINT(20) NOT NULL,
	assignee_id BIGINT(20),
	state_id BIGINT(20),
	name VARCHAR(255),
	description VARCHAR(255),
	state VARCHAR(255),
	PRIMARY KEY (id),
	FOREIGN KEY (assignee_id) REFERENCES user(id),
	FOREIGN KEY (state_id) REFERENCES state(id)
);

CREATE TABLE IF NOT EXISTS workflow_step(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	version BIGINT(20) NOT NULL,
	src_state_id BIGINT(20) NOT NULL,
	transition_id BIGINT(20) NOT NULL,
	dst_state_id BIGINT(20) NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT uc_src_state_transition UNIQUE(src_state_id,transition_id),
	FOREIGN KEY (src_state_id) REFERENCES state(id),
	FOREIGN KEY (dst_state_id) REFERENCES state(id),
	FOREIGN KEY (transition_id) REFERENCES transition(id)
);

CREATE TABLE IF NOT EXISTS action_transition(
	action_id INT(20) NOT NULL,
	transition_id BIGINT(20) NOT NULL,
	FOREIGN KEY (transition_id) REFERENCES transition(id)
);

CREATE TABLE IF NOT EXISTS entry_state_id(
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	entry_state_id BIGINT(20) NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT uc_entry_state_id UNIQUE(entry_state_id),
	FOREIGN KEY (entry_state_id) REFERENCES state(id)
);

/* ON DELETE CASCADE doesn't make sense in this case, but
what we should do when a User will get deleted?...
The issue would point to a non-existing user's ID until it would get re-assigned
*/

/* Should I create a dedicated schema (CREATE SCHEMA IF NOT EXISTS schema_name)? 
or just stay with the default assuming that it's always there? - easier for now at least */