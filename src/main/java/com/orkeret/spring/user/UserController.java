package com.orkeret.spring.user;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.common.BaseController;
import com.orkeret.spring.exception.ConflictException;
import com.orkeret.spring.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/users")
public class UserController extends BaseController<UserDto> {
	// no need to use If-Match on the GET ("Read") operations (only interested to solve the "Lost Update Problem" at the moment)
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserResourceAssembler assembler;
	
	@GetMapping
	public Collection<Resource<UserDto>> getAllUsers() {
		logger.info("request received to fetch all users");
		
		Collection<Resource<UserDto>> userDtoResources = new ArrayList<>();
		userService.getAllUsers().forEach(user -> userDtoResources.add(assembler.toResource(user)));
		
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(userDtoResources, ... );

		return userDtoResources;
	}
	
	@PostMapping
	public ResponseEntity<Resource<UserDto>> createUser(@RequestBody @Valid UserDto userDto) {
		logger.info("request to create new user has been received");
		
		User updatedUser = userService.updateOrCreateUser(userDto.toDomainModel());
		
		return createResponseEntity(assembler.toResource(updatedUser), updatedUser.getVersion(), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Resource<UserDto>> getUser(@PathVariable("id") Long id) {
		logger.info("request received to fetch user with ID: " + id);
		validateID(id);
		User user = userService.getUser(id).orElseThrow(() -> new ResourceNotFoundException("User with ID: " + id + " does not exist"));
		
		return createResponseEntity(assembler.toResource(user), user.getVersion(), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Resource<UserDto>> updateUser(WebRequest request, @RequestBody @Valid UserDto userDto, @PathVariable("id") Long id) {
		logger.info("request received to update user with ID: " + id);
		validateID(id);
		// note that the userDto.id would get ignored - the ID from the pathParam would be the only ID that will be taken into account
		// consider creating a thinner version of UserDto that doesn't contain an ID field
		
		return userService.getUser(id).map(existingUser -> { // user exists!
			validateETag(request, existingUser.getVersion());
			
			// need to make sure that all the fields (besides the ID and Version) are copied over from the dto to the non-dto object
			existingUser.setEmail(userDto.getEmail());
			existingUser.setName(userDto.getName());
			
			try {
				User updatedUser = userService.updateOrCreateUser(existingUser);
				return createResponseEntity(assembler.toResource(updatedUser), updatedUser.getVersion(), HttpStatus.OK);
			} catch (OptimisticLockingFailureException e){
				/* 
				 * "Optimistic locking" is more scalable than "Pessimistic locking" when dealing with a
				 * highly concurrent environment. However pessimistic locking is a better solution for situations
				 * where the possibility of simultaneous updates to the same data by multiple sources is common
				 * 
				 * For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates.
				 * if proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:
				 * Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
				 * Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)
				 */
				throw new ConflictException("Failed updating user ID: " + id + ". Another update taken place in the same time, please re-fetch the resource, apply the changes and try again", e);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("You are trying to edit a non-existing user [User ID: " + id + "]"));
	}
	
	@DeleteMapping("/{id}")
	public void deleteUser(@PathVariable("id") Long id) {
		logger.info("request received to delete user with ID: " + id);
		validateID(id);
		 // trying to delete a user that has associated issues with fail and issue a 400 response with a message that says that
		userService.deleteUser(id);
	}
}
