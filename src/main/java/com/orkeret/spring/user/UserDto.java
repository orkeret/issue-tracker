package com.orkeret.spring.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

//Consider creating a dummy user which all of the unassigned issues would be assigned (currently I'm not allowing unassigned issues)
public class UserDto {

	@Min(1)
	private Long id;
	
	@Email(message = "Invalid Email format")
	@NotEmpty(message = "Email is a required field")
	private String email;
	
	@NotEmpty(message = "Name is a required field")
	private String name;
	
	public UserDto() { }
	
	public UserDto(User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.name = user.getName();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public User toDomainModel() {
		// we would ignore the id that the client sends us (in the payload, we would use the one from the path param) , we still keep this field to export the ID to the client
		return new User(email, name);
	}
	
	public String toString() {
		return "User [id: " + id + ", name: " + name + ", email: " + email + "]";
	}
}