package com.orkeret.spring.user;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.ResourceNotFoundException;

// consider to add an inMemoryModel for optimising read OPs

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;

	public Iterable<User> getAllUsers() {
		return userDao.findAll();
	}
	
	public Optional<User> getUser(Long id) {
		return userDao.findById(id);
	}
	
	public User updateOrCreateUser(User user) {
		return userDao.save(user);
	}
	
	/*
	 * easier and safe enough to implement it that way for now (could re-iterate on it later)
	 * by that point we know that it's a valid id, the question is if it belongs to an existing user
	 * and if this user is associated with any entities (currently just issues).
	 * If it's an existing user and it's not associated with any entities we would just delete it.
	 * If it's an existing user and the user is associated with any entities,
	 * we would throw an exception that would lead to a 400 bad-request response as it's a data integrity violation.
	 * If it's a non-existing user we would throw an exception that would lead to a 404 not found.
	 * 
	 */
	public void deleteUser(Long id) {
		try {
			userDao.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("User with ID: " + id + " does not exist", e);
		} catch (DataIntegrityViolationException e) {
			throw new BadRequestException("Cannot delete user [User ID: " + id + "]. The user still has issues assigned, please remove those first and try again.", e);
		}
	}

}
