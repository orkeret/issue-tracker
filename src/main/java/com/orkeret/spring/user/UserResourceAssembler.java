package com.orkeret.spring.user;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class UserResourceAssembler implements ResourceAssembler<User, Resource<UserDto>> {

	@Override
	public Resource<UserDto> toResource(User user) {

		return new Resource<>(new UserDto(user),
			linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel(),
			linkTo(methodOn(UserController.class).getAllUsers()).withRel("users"));
	}
}