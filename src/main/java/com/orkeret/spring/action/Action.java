package com.orkeret.spring.action;

import java.util.function.Function;

import com.orkeret.spring.issue.Issue;

public class Action {

	private final int id;
	private final String name;
	private final boolean defaultAction;
	private final Function<Issue, Boolean> function;

	public Action(int id, String name, boolean defaultAction, Function<Issue, Boolean> function) {
		this.id = id;
		this.name = name;
		this.defaultAction = defaultAction;
		this.function = function;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isDefaultAction() {
		return defaultAction;
	}

	public Function<Issue, Boolean> getFunction() {
		return function;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Action)) {
			return false;
		}
		
		Action other = (Action)obj;
		return id == other.id && defaultAction == other.defaultAction;
	}
}
