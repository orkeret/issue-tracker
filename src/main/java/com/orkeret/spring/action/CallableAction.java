package com.orkeret.spring.action;

import java.util.Objects;
import java.util.concurrent.Callable;

import com.orkeret.spring.issue.Issue;

public class CallableAction implements Callable<Boolean> {

	private final Action action;
	private final Issue issue;

	public CallableAction(Action action, Issue issue) {
		if (action == null || issue == null) {
			throw new IllegalArgumentException("action and issue are required arguments and cannot be null.");
		}
		
		this.action = action;
		this.issue = issue;
	}
	
	@Override
	public Boolean call() throws Exception {
		return action.getFunction().apply(issue);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CallableAction)) {
			return false;
		}
		
		CallableAction other = (CallableAction)obj;
		return Objects.equals(action, other.action) && Objects.equals(issue, other.issue);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(action, issue);
	}

}
