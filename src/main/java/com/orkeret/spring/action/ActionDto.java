package com.orkeret.spring.action;

public class ActionDto {

	private int id;
	private String name;
	
	public ActionDto(Action action) {
		this.id = action.getId();
		this.name = action.getName();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
