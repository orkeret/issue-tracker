package com.orkeret.spring.action;

import java.util.Collection;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orkeret.spring.common.BaseController;

/*
 * Default actions are inner actions and wouldn't get exported, this class ignores them as they are inner private concept.
 * This controller only exports and associates "dynamically associated" actions
 */
@RestController
@RequestMapping("/actions")
public class ActionController extends BaseController<ActionDto> {

	private static Logger logger = LoggerFactory.getLogger(ActionController.class);
	
	@Autowired
	private ActionManager actionManager;

	@Autowired
	private ActionResourceAssembler assembler;
	
	@GetMapping
	public Collection<Resource<ActionDto>> getAllActions() {
		logger.info("request received to fetch all dynamically associable actions");
		
		return actionManager.getAllDynamicallyAssociableActions().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	public Resource<ActionDto> getAction(@PathVariable("id") Integer id) {
		logger.info("request received to fetch action with ID: " + id);
		int firstID = 0;
		validateID(id, firstID);
		
		return assembler.toResource(actionManager.getDynamicallyAssociableAction(id));
	}

}
