package com.orkeret.spring.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.issue.Issue;

@Service
public class ActionManager {
	private static Logger logger = LoggerFactory.getLogger(ActionManager.class);
	
	private List<Action> defaultActions;
	private List<Action> dynamicallyAssociableActions;
	
	@PostConstruct
	public void init() {
		registerAllDefaultActions();
		registerAllDynamicActions();
	}
	
	public Collection<Action> getAllDynamicallyAssociableActions() {
		return Collections.unmodifiableCollection(dynamicallyAssociableActions);
	}
	
	public Collection<Action> getAllDefaultActions() {
		return Collections.unmodifiableCollection(defaultActions);
	}
	
	public Action getDynamicallyAssociableAction(int id) {
		if (id < 0 || id >= dynamicallyAssociableActions.size()) {
			throw new ResourceNotFoundException("Action with ID: " + id + " does not exist");
		}
		
		return dynamicallyAssociableActions.get(id);
	}
	
	private void registerAllDefaultActions() {
		defaultActions = new ArrayList<>();
		
		addDefaultAction("samplePostProcess", this::samplePostProcess);
		// add here all the default actions that would always get triggered for any given transition
	}
	
	private void registerAllDynamicActions() {
		dynamicallyAssociableActions = new ArrayList<>();
		
		addDynamicAction("sampleSendEmail", this::sampleSendEmail);
		/*
		 * add here all non-default actions - those actions would only be added to the action's registry and wouldn't get triggered automatically
		 * unless the user configured otherwise.
		 * a user could dynamically associate any given transition with a subset of these associable actions which would cause this subset of
		 * actions to get triggered if such transition occurs (that would be on top of the default actions).
		 * 
		 * ###########################################################################################
		 * the order of which the actions are registered sets their IDs - therefore it's important to:
		 *  1) never delete an action
		 *  2) if you need to add a new action - put it in the end
		 *  ###########################################################################################
		 *  
		 *  
		 *  future enhancement - dummy proof it from other developers that could ignore those 2 commandments
		 *  
		 *  future enhancement - add "disabled" or "deactivated" flag so we would be able to still keep "old" actions for audit purposes but wouldn't
		 *  let a user to dynamically register any transition with disabled actions.
		 *  we then could let project owners know when an action becomes disabled etc.
		 *  
		 */
	}
	
	private void addDefaultAction(String name, Function<Issue, Boolean> function) {
		int currSize = defaultActions.size();
		boolean defaultAction = true;
		defaultActions.add(new Action(currSize, name, defaultAction, function));
	}
	
	private void addDynamicAction(String name, Function<Issue, Boolean> function) {
		int currSize = dynamicallyAssociableActions.size();
		boolean defaultAction = false;
		dynamicallyAssociableActions.add(new Action(currSize, name, defaultAction, function));
	}
	
	private Boolean sampleSendEmail(Issue issue) {
		logger.info("'sample send email' action has been triggered for issue [ID: " + issue.getId() + ", name: " + issue.getName() + "]");
		return Boolean.TRUE;
	}
	
	private Boolean samplePostProcess(Issue issue) {
		logger.info("'sample post-process' action has been triggered for issue [ID: " + issue.getId() + ", name: " + issue.getName() + "]");
		return Boolean.TRUE;
	}
	
}
