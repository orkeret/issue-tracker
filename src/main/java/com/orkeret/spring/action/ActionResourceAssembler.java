package com.orkeret.spring.action;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class ActionResourceAssembler implements ResourceAssembler<Action, Resource<ActionDto>> {

	@Override
	public Resource<ActionDto> toResource(Action action) {

		return new Resource<>(new ActionDto(action),
			linkTo(methodOn(ActionController.class).getAction(action.getId())).withSelfRel(),
			linkTo(methodOn(ActionController.class).getAllActions()).withRel("actions"));
	}
}