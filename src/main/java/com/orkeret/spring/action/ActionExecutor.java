package com.orkeret.spring.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

import com.orkeret.spring.issue.Issue;
import com.orkeret.spring.transition.Transition;

@Service
public class ActionExecutor {

	private static Logger logger = LoggerFactory.getLogger(ActionExecutor.class);
	
	@Autowired
	private AsyncTaskExecutor taskExecutor;
	
	@Autowired
	private ActionManager actionManager;
	
	public void executeActionsAfterTransition(Issue issue, Transition transition) {
		assert issue != null;
		assert transition != null;
		
		logger.info("Starting execution of actions after transition [ID: " + transition.getId() + ", name: " + transition.getName() + "] "
				+ "for issue [ID: " + issue.getId() + ", name: " + issue.getName() + "]");
		
		try {
			for (Action defaultAction : actionManager.getAllDefaultActions()) {
				taskExecutor.submit(new CallableAction(defaultAction, issue));
			}
			
			for (Integer actionId : transition.getAssociatedActionIDs()) {
				Action action = actionManager.getDynamicallyAssociableAction(actionId);
				taskExecutor.submit(new CallableAction(action, issue));
			}
		} catch (Exception e) {
			logger.error("Failed executing actions after transition [ID: " + transition.getId() + ", name: " + transition.getName() + "] "
					+ "for issue [ID: " + issue.getId() + ", name: " + issue.getName() + "]", e);
		}
	}
	
}
