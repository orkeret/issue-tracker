package com.orkeret.spring.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="entry_state_id")
public class EntryState {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="entry_state_id")
	private Long entryStateID;
	
	public EntryState(Long entryStateID) {
		this.entryStateID = entryStateID;
	}
	
	public EntryState() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getEntryStateID() {
		return entryStateID;
	}

	public void setEntryStateID(Long entryStateID) {
		this.entryStateID = entryStateID;
	}
	
}
