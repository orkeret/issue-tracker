package com.orkeret.spring.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.orkeret.spring.state.State;
import com.orkeret.spring.state.StateDao;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.transition.TransitionDao;
import com.orkeret.spring.workflow.WorkflowStep;
import com.orkeret.spring.workflow.WorkflowStepDao;

@Component
public class WorkflowCommandLineRunner implements CommandLineRunner {

	private static Logger logger = LoggerFactory.getLogger(WorkflowCommandLineRunner.class);
	
	@Autowired
	private StateDao stateDao;
	
	@Autowired
	private TransitionDao transitionDao;
	
	@Autowired
	private WorkflowStepDao workflowTransitionDao;
	
	@Autowired
	private EntryStateDao entryStateDao;
	
	@Override
	public void run(String... args) throws Exception {
		/*
		 * could consider to change the approach to create a "one-time set" flag and to set it after the first initialization.
		 * could do it with a new table (new dummy @Entity and then to just check if this entity's repo is empty, if so execute this init state
		 * and persist a new dummy instance then next time we would see that the repo isn't empty so we would know to skip this init stage.
		 */
		
		/*
		 * since I'm persisting some states, transitions and workflow steps and also don't let a user to delete the special state ("Entry State") then
		 * it would always stay there after the initialising meaning that it's enough to check that there is any states persisted.
		 */
		if (stateDao.count() > 0) {
			logger.info("Skipping workflow initialization as it has already been initialized.");
			return;
		}
		
		logger.info("Initializing workflow...");
		State entryState = stateDao.save(new State("ENTRY STATE"));
		State openState = stateDao.save(new State("OPEN"));
		State inProgressState = stateDao.save(new State("IN PROGRESS"));
		State closedState = stateDao.save(new State("CLOSED"));
		State reopenedState = stateDao.save(new State("REOPENED"));
		State resolvedState = stateDao.save(new State("RESOLVED"));
		
		Transition startProgressTransition = transitionDao.save(new Transition("START PROGRESS"));
		Transition stopProgressTransition = transitionDao.save(new Transition("STOP PROGRESS"));
		Transition resolveIssueTransition = transitionDao.save(new Transition("RESOLVE ISSUE"));
		Transition closeIssueTransition = transitionDao.save(new Transition("CLOSE ISSUE"));
		Transition reopenIssueTransition = transitionDao.save(new Transition("REOPEN ISSUE"));
		Transition createIssueTransition = transitionDao.save(new Transition("CREATE ISSUE"));
		
		// Entry to the FSM/workflow
		workflowTransitionDao.save(new WorkflowStep("(entry, create) -> open", entryState, createIssueTransition, openState));
		
		// Open state
		workflowTransitionDao.save(new WorkflowStep("(open, start) -> in progress", openState, startProgressTransition, inProgressState));
		workflowTransitionDao.save(new WorkflowStep("(open, resolve) -> resolved", openState, resolveIssueTransition, resolvedState));
		workflowTransitionDao.save(new WorkflowStep("(open, close) -> closed", openState, closeIssueTransition, closedState));
		
		// In progress state
		workflowTransitionDao.save(new WorkflowStep("(in progress, stop) -> open", inProgressState, stopProgressTransition, openState));
		workflowTransitionDao.save(new WorkflowStep("(in progress, resolve) -> resolved", inProgressState, resolveIssueTransition, resolvedState));
		workflowTransitionDao.save(new WorkflowStep("(in progress, close) -> closed", inProgressState, closeIssueTransition, closedState));
		
		// Closed state
		workflowTransitionDao.save(new WorkflowStep("(closed, reopen) -> reopened", closedState, reopenIssueTransition, reopenedState));
		
		// Reopened state
		workflowTransitionDao.save(new WorkflowStep("(reopened, start) -> in progress", reopenedState, startProgressTransition, inProgressState));
		workflowTransitionDao.save(new WorkflowStep("(reopened, resolve) -> resolved", reopenedState, resolveIssueTransition, resolvedState));
		workflowTransitionDao.save(new WorkflowStep("(reopened, close) -> closed", reopenedState, closeIssueTransition, closedState));
		
		// Resolve state
		workflowTransitionDao.save(new WorkflowStep("(resolved, close) -> closed", resolvedState, closeIssueTransition, closedState));
		workflowTransitionDao.save(new WorkflowStep("(resolved, reopen) -> reopened", resolvedState, reopenIssueTransition, reopenedState));
		
		entryStateDao.save(new EntryState(entryState.getId()));
		
		logger.info("Workflow initialization has been completed.");
	}

}
	