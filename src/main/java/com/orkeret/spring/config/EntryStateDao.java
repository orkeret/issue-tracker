package com.orkeret.spring.config;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface EntryStateDao extends CrudRepository<EntryState, Long> {
	
	@Query("select s from EntryState s")
	public List<EntryState> findEntryStates();
}
