package com.orkeret.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolConfiguration {
 
    @Bean
    public AsyncTaskExecutor threadPoolTaskExecutor() {
 
        // should consider further additional configuration
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(1000); // consider to change the maxPoolSize
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
 
        return executor;
    }
 
}