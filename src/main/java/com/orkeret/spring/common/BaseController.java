package com.orkeret.spring.common;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.PreconditionFailedException;

public class BaseController<T> {
	
	protected ResponseEntity<Resource<T>> createResponseEntity(Resource<T> exportableEntity, Long version, HttpStatus httpStatus) {
		return ResponseEntity.status(httpStatus)
				.eTag(escapeVersion(version))
				.body(exportableEntity);
	}
	
	protected static void validateID(Number requestedID) {
		int firstID = 1;
		validateID(requestedID, firstID);
	}
	
	protected static void validateID(Number requestedID, int firstID) {
		String fieldName = null;
		validateID(requestedID, fieldName, firstID);
	}
	
	protected static void validateID(Number requestedID, String fieldName) {
		int firstID = 1;
		validateID(requestedID, fieldName, firstID);
	}
	
	protected static void validateID(Number requestedID, String fieldName, int firstID) {
		if (requestedID == null) { // this null check might be redundant for some cases, consider to have additional method without it
			throw new BadRequestException((fieldName != null ? fieldName : "ID") + " Cannot be null");
		}
		
		if (requestedID.longValue() < firstID) {
			throw new BadRequestException((fieldName != null ? fieldName : "ID") + " should be greater or equal to " + firstID + ". ID requested is: " + requestedID);
		}
	}
	
	protected static void validateETag(WebRequest request, Long requestVersion) {
		String ifMatchValue = request.getHeader(HttpHeaders.IF_MATCH);
		if (ifMatchValue == null || ifMatchValue.isEmpty()) {
	    	throw new BadRequestException("Missing " + HttpHeaders.IF_MATCH + " header. please add it and then try again");
    	}
		
		String expectedVersion =  escapeVersion(requestVersion);
		if (!ifMatchValue.equals(expectedVersion)) {
			throw new PreconditionFailedException("Resource out of date. expected version: " + expectedVersion + ", received version: " + ifMatchValue);
		}
	}
	
	protected static boolean isETagExist(WebRequest request) {
		String ifMatchValue = request.getHeader(HttpHeaders.IF_MATCH);
		return ifMatchValue != null && !ifMatchValue.isEmpty();
	}
	
	private static String escapeVersion(Long version) {
		return "\"" + version + "\"";
	}
}
