package com.orkeret.spring.exception;

public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5768201336632810938L;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException(String message, Throwable t) {
		super(message, t);
	}

}
