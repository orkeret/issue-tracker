package com.orkeret.spring.exception;

public class ConflictException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1676662185960854402L;

	public ConflictException(String message) {
		super(message);
	}

	public ConflictException(String message, Throwable t) {
		super(message, t);
	}
}
