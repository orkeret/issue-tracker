package com.orkeret.spring.exception;

public class PreconditionFailedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 359521071504256071L;
	
	public PreconditionFailedException(String message) {
		super(message);
	}
}