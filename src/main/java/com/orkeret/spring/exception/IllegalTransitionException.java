package com.orkeret.spring.exception;

public class IllegalTransitionException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7344113601253012089L;

	public IllegalTransitionException(String message) {
		super(message);
	}
}
