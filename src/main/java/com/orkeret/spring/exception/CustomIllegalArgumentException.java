package com.orkeret.spring.exception;

public class CustomIllegalArgumentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9124753419672748599L;

	public CustomIllegalArgumentException(String message) {
		super(message);
	}

}
