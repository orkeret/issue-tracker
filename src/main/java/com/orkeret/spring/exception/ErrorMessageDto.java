package com.orkeret.spring.exception;

class ErrorMessageDto {
	private final String message;
	
	ErrorMessageDto(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
