package com.orkeret.spring.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {
	/*
	 * should *not* extend from the Abstract class ResponseEntityExceptionHandler - otherwise the handling wouldn't propagate to this class 
	 * and we wouldn't get decent logging and more fined grained handling that fits our purposes. E.g if a client is missing a RequestParam
	 * it should get a 400 and *not* 500 response.
	 */
	private static Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
	
	@ExceptionHandler({ BadRequestException.class, HttpMessageNotReadableException.class})
	public ResponseEntity<ErrorMessageDto> handleBadRequestException(Exception ex, WebRequest request) {
		logger.warn("Bad Request occurred", ex);
	    return ResponseEntity.badRequest()
	    		.contentType(MediaType.APPLICATION_JSON)
	    		.body(new ErrorMessageDto(ex.getMessage()));
	}
	
	@ExceptionHandler({ ResourceNotFoundException.class })
	public ResponseEntity<ErrorMessageDto> handleResourceNotFound(Exception ex, WebRequest request) {
		logger.warn("Resource not found", ex);
		 return ResponseEntity.status(HttpStatus.NOT_FOUND)
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
	@ExceptionHandler({ IllegalTransitionException.class })
	public ResponseEntity<ErrorMessageDto> handleIllegalTransitionException(Exception ex, WebRequest request) {
		logger.warn("Illegal transition was requested", ex);
		 return ResponseEntity.badRequest()
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
	// the main reason for that is to make sure that those few exceptions mean that it's a bad-request and not a server-error
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class, ConversionNotSupportedException.class, MissingServletRequestParameterException.class, MethodArgumentNotValidException.class, CustomIllegalArgumentException.class})
	public ResponseEntity<ErrorMessageDto> handleArgumentValidationException(Exception ex, WebRequest request) {
		logger.warn("Argument validation exception has been thrown", ex);
		 return ResponseEntity.badRequest()
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
	@ExceptionHandler({ PreconditionFailedException.class })
	public ResponseEntity<ErrorMessageDto> handlePrecondtionFailedException(Exception ex, WebRequest request) {
		logger.warn("PrecondtionFailedException occoured", ex);
		 return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED)
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
	@ExceptionHandler({ ConflictException.class })
	public ResponseEntity<ErrorMessageDto> handleConflictException(Exception ex, WebRequest request) {
		logger.warn("Conflict occoured", ex);
		 return ResponseEntity.status(HttpStatus.CONFLICT)
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<ErrorMessageDto> handleUncaughtException(Exception ex, WebRequest request) {
		logger.error("Exception has been thrown", ex);
		 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				 .contentType(MediaType.APPLICATION_JSON)
				 .body(new ErrorMessageDto(ex.getMessage()));
	}
	
}