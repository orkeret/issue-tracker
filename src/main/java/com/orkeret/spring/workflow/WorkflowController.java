package com.orkeret.spring.workflow;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.common.BaseController;
import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.ConflictException;
import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.issue.Issue;
import com.orkeret.spring.issue.IssueService;
import com.orkeret.spring.state.State;
import com.orkeret.spring.state.StateService;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.transition.TransitionService;

@RestController
@RequestMapping("/workflow")
public class WorkflowController extends BaseController<WorkflowStepDto> {

	private static Logger logger = LoggerFactory.getLogger(WorkflowController.class);
	
	@Autowired
	private WorkflowService workflowService;
	
	@Autowired
	private WorkflowStepResourceAssembler assembler;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private TransitionService transitionService;
	
	@Autowired
	private IssueService issueService;
	
	// issueId parameter is optional. if provided it would filter out the results accordingly
	@GetMapping
	public Collection<Resource<WorkflowStepDto>> getAvailableWorkflowSteps(@RequestParam(value="issueId", required=false) Long issueId) {
		Iterable<WorkflowStep> workflowSteps;
		
		if (issueId != null) {
			logger.info("request received to fetch all Workflow steps available for issue ID: " + issueId);
			validateID(issueId, "issueId");
			
			Issue issue = issueService.getIssue(issueId).orElseThrow(() -> new BadRequestException("This request referencing a non-existing issue [Issue ID: " + issueId + "]"));
			workflowSteps = workflowService.getAvailableWorkflowStepsFromState(issue.getState().getId());
		} else {
			logger.info("request received to fetch all Workflow steps");
			workflowSteps = workflowService.getAllWorkflowSteps();
		}
		
		Collection<Resource<WorkflowStepDto>> workflowStepDtoResources = new ArrayList<>();
		workflowSteps.forEach(ws -> workflowStepDtoResources.add(assembler.toResource(ws)));
		
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(workflowStepDtoResources, ... );

		return workflowStepDtoResources;
	}
	
	@PostMapping
	public ResponseEntity<Resource<WorkflowStepDto>> createWorkflowStep(@RequestBody @Valid WorkflowStepDto workflowStepDto) {
		logger.info("request to create new workflow step has been received");
		
		// consider to aggregate errors so I could throw a more informative message (e.g.: sourceState and destinationState don't exist)
		// at the moment it throws an exception for the first missing entity that it would encounter
		State sourceState = fetchState(workflowStepDto.getSourceStateId());
		State destinationState = fetchState(workflowStepDto.getDestinationStateId());
		Transition transition = fetchTransition(workflowStepDto.getTransitionId());
		
		WorkflowStep workflowStep = workflowService.updateOrCreateWorkflowStep(workflowStepDto.toDomainModel(sourceState, transition, destinationState));
		
		return createResponseEntity(assembler.toResource(workflowStep), workflowStep.getVersion(), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Resource<WorkflowStepDto>> getWorkflowStep(@PathVariable("id") Long id) {
		logger.info("request received to fetch Workflow step with ID: " + id);
		validateID(id);
		
		WorkflowStep workflowStep = workflowService.getWorkflowStep(id).orElseThrow(() -> new ResourceNotFoundException("workflow step with ID: " + id + " does not exist"));

		return createResponseEntity(assembler.toResource(workflowStep), workflowStep.getVersion(), HttpStatus.OK);
	}
	
	/*
	 * I didn't want to strict it to just one workflow step which would be via "CREATE ISSUE" transition.
	 * Now with this approach we could have multiple initial workflow steps, e.g. from non-existing state we could use "CREATE ISSUE" transition to set
	 * the initial state to "OPEN" or potentially could have in the future "CREATE ISSUE AND MOVE TO IN PROGRESS" transition which could set the initial
	 * state to "IN PROGRESS" with just one super duper transition/
	 */
	@GetMapping("/initial")
	public Collection<Resource<WorkflowStepDto>> getInitialWorkflowSteps() {
		logger.info("request received to fetch initial Workflow steps");
		
		Collection<WorkflowStep> initialWorkflowSteps = workflowService.getInitialWorkflowSteps();
		Collection<Resource<WorkflowStepDto>> workflowStepDtoResources = new ArrayList<>();
		initialWorkflowSteps.forEach(ws -> workflowStepDtoResources.add(assembler.toResource(ws)));
		
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(workflowStepDtoResources, ... );

		return workflowStepDtoResources;
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Resource<WorkflowStepDto>> updateWorkflowStep(WebRequest request, @RequestBody @Valid WorkflowStepDto workflowStepDto, @PathVariable("id") Long id) {
		logger.info("request received to update workflow step with ID: " + id);
		validateID(id);
		// note that the workflowStepDto.id would get ignored - the ID from the pathParam would be the only ID that will be taken into account
		// consider creating a thinner version of workflowStepDto that doesn't contain an ID field
		
		return workflowService.getWorkflowStep(id).map(existingWorfklowStep -> { // workflow step exists!
			validateETag(request, existingWorfklowStep.getVersion());
			
			// need to make sure that all the fields (besides the ID and Version) are copied over from the dto to the non-dto object
			existingWorfklowStep.setName(workflowStepDto.getName());
			existingWorfklowStep.setSourceState(fetchState(workflowStepDto.getSourceStateId()));
			existingWorfklowStep.setTransition(fetchTransition(workflowStepDto.getTransitionId()));
			existingWorfklowStep.setDestinationState(fetchState(workflowStepDto.getDestinationStateId()));
			
			try {
				WorkflowStep updatedWorkflowStep = workflowService.updateOrCreateWorkflowStep(existingWorfklowStep);
				return createResponseEntity(assembler.toResource(updatedWorkflowStep), updatedWorkflowStep.getVersion(), HttpStatus.OK);
			} catch (OptimisticLockingFailureException e){
				/* 
				 * "Optimistic locking" is more scalable than "Pessimistic locking" when dealing with a
				 * highly concurrent environment. However pessimistic locking is a better solution for situations
				 * where the possibility of simultaneous updates to the same data by multiple sources is common
				 * 
				 * For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates.
				 * if proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:
				 * Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
				 * Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)
				 */
				throw new ConflictException("Failed updating workflow step ID: " + id + ". Another update taken place in the same time, please re-fetch the resource, apply the changes and try again", e);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("You are trying to edit a non-existing workflow step [Workflow step ID: " + id + "]"));
	}

	@DeleteMapping("/{id}")
	public void deleteWorkflowStep(@PathVariable("id") Long id) {
		logger.info("request received to delete workflow step with ID: " + id);
		validateID(id);
		
		workflowService.deleteWorkflowStep(id);
	}

	private State fetchState(Long stateId) {
		State state = stateService.getState(stateId).orElseThrow(() -> new BadRequestException("This workflow step is referencing a non-existing state [State ID: " + stateId + "]"));
		return state;
	}
	
	private Transition fetchTransition(Long transitionId) {
		Transition transition = transitionService.getTransition(transitionId).orElseThrow(() -> new BadRequestException("This workflow step is referencing a non-existing transition [Transition ID: " + transitionId + "]"));
		return transition;
	}
}
