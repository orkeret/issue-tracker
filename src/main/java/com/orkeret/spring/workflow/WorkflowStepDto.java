package com.orkeret.spring.workflow;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.orkeret.spring.state.State;
import com.orkeret.spring.transition.Transition;

public class WorkflowStepDto {
	@Min(1)
	private Long id;
	
	@NotEmpty(message = "Name is a required field")
	private String name;

	@Min(1)
	@NotNull(message = "sourceStateId is a required field")
	private Long sourceStateId;
	
	@Min(1)
	@NotNull(message = "transitionId is a required field")
	private Long transitionId;
	
	@Min(1)
	@NotNull(message = "destinationStateId is a required field")
	private Long destinationStateId;
	
	public WorkflowStepDto() { }
	
	public WorkflowStepDto(WorkflowStep workflowTransition) {
		this.id = workflowTransition.getId();
		this.name = workflowTransition.getName();
		this.sourceStateId = workflowTransition.getSourceState().getId();
		this.transitionId = workflowTransition.getTransition().getId();
		this.destinationStateId = workflowTransition.getDestinationState().getId();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSourceStateId() {
		return sourceStateId;
	}

	public void setSourceStateId(Long sourceStateId) {
		this.sourceStateId = sourceStateId;
	}

	public Long getTransitionId() {
		return transitionId;
	}

	public void setTransitionId(Long transitionId) {
		this.transitionId = transitionId;
	}

	public Long getDestinationStateId() {
		return destinationStateId;
	}

	public void setDestinationStateId(Long destinationStateId) {
		this.destinationStateId = destinationStateId;
	}
	
	public WorkflowStep toDomainModel(State sourceState, Transition transition, State destinationState) {
		// we would ignore the id that the client sends us (in the payload, we would use the one from the path param) , we still keep this field to export the ID to the client
		return new WorkflowStep(name, sourceState, transition, destinationState);
	}
	
	public String toString() {
		return "Workflow-step [id: " + id + ", name: " + name + ", sourceStateId: " + sourceStateId + ", transitionId: " + transitionId + ", destinationStateId: " + destinationStateId + "]";
	}
	
}
