package com.orkeret.spring.workflow;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.orkeret.spring.state.State;
import com.orkeret.spring.transition.Transition;

@Entity
@Table(name="workflow_step")
public class WorkflowStep {
	// could add desc.
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String name;

	@Version
	private Long version; // serves as its optimistic lock. will be part of handling the "Lost Update Problem"
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="src_state_id")
	private State sourceState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="transition_id")
	private Transition transition;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="dst_state_id")
	private State destinationState;
	
	WorkflowStep() { }
	
	public WorkflowStep(Long id, String name, State sourceState, Transition transition, State destinationState) {
		this.id = id;
		this.name = name;
		this.sourceState = sourceState;
		this.transition = transition;
		this.destinationState = destinationState;
	}
	
	public WorkflowStep(String name, State sourceState, Transition transition, State destinationState) {
		this(null, name, sourceState, transition, destinationState);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public State getSourceState() {
		return sourceState;
	}
	
	public void setSourceState(State sourceState) {
		this.sourceState = sourceState;
	}
	
	public Transition getTransition() {
		return transition;
	}
	
	public void setTransition(Transition transition) {
		this.transition = transition;
	}
	
	public State getDestinationState() {
		return destinationState;
	}
	
	public void setDestinationState(State destinationState) {
		this.destinationState = destinationState;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof WorkflowStep)) {
			return false;
		}
		
		return Objects.equals(id, ((WorkflowStep) obj).id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
