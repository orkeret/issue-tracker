package com.orkeret.spring.workflow;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface WorkflowStepDao extends CrudRepository<WorkflowStep, Long> {
	
	@Query("select w from WorkflowStep w where w.sourceState.id = ?#{[0]}")
	List<WorkflowStep> findWorkflowStepsBySourceState(Long stateId);
	
	@Query("select w from WorkflowStep w where w.sourceState.id = ?#{[0]} and w.transition.id = ?#{[1]}")
	WorkflowStep findWorkflowStepBySourceStateAndTransition(Long srcStateId, Long transitionId);
}
