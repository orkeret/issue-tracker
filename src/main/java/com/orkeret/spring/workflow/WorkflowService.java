package com.orkeret.spring.workflow;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.IllegalTransitionException;
import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.state.State;
import com.orkeret.spring.state.StateService;

@Service
public class WorkflowService {

	@Autowired
	private WorkflowStepDao workflowStepDao;
	
	@Autowired
	private StateService stateService;
	
	public Iterable<WorkflowStep> getAllWorkflowSteps() {
		return workflowStepDao.findAll();
	}

	public Optional<WorkflowStep> getWorkflowStep(Long id) {
		return workflowStepDao.findById(id);
	}

	public WorkflowStep updateOrCreateWorkflowStep(WorkflowStep workflowTransition) {
		try {
			return workflowStepDao.save(workflowTransition);
		} catch (DataIntegrityViolationException e) {
			throw new BadRequestException("Cannot persist workflow step as it causes data integrity violation. There might be an existing"
					+ " workflow step with the same source state and transition.", e);
		}
	}
	
	public void deleteWorkflowStep(Long id) {
		try {
			workflowStepDao.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Workflow step with ID: " + id + " does not exist", e);
		}
	}
	
	public Collection<WorkflowStep> getAvailableWorkflowStepsFromState(Long stateId) {
		return workflowStepDao.findWorkflowStepsBySourceState(stateId);
	}
	
	public Collection<WorkflowStep> getInitialWorkflowSteps() {
		 return getAvailableWorkflowStepsFromState(stateService.getEntryStateID());
	}
	
	public State getNextState(Long stateId, Long transitionId) {
		WorkflowStep step = workflowStepDao.findWorkflowStepBySourceStateAndTransition(stateId, transitionId);
		if (step == null) {
			throw new IllegalTransitionException("transition represnted by transitionId: " + transitionId + " is not allowed from the state which represented by stateId: " + stateId);
		}
		
		return step.getDestinationState();
	}
	
	/**
	 * This method will calculate the next state given the supplied transitionId and entry-state
	 * @param transitionId - ID of the transition
	 * @return the next state
	 */
	public State getNextState(Long transitionId) {
		return getNextState(stateService.getEntryStateID(), transitionId);
	}

}
