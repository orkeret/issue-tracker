package com.orkeret.spring.workflow;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class WorkflowStepResourceAssembler implements ResourceAssembler<WorkflowStep, Resource<WorkflowStepDto>> {

	@Override
	public Resource<WorkflowStepDto> toResource(WorkflowStep workflowStep) {

		return new Resource<>(new WorkflowStepDto(workflowStep),
			linkTo(methodOn(WorkflowController.class).getWorkflowStep(workflowStep.getId())).withSelfRel(),
			linkTo(methodOn(WorkflowController.class).getAvailableWorkflowSteps(null)).withRel("workflow"));
	}
}
