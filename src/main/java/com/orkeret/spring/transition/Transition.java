package com.orkeret.spring.transition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="transition")
public class Transition {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version; // serves as its optimistic lock. will be part of handling the "Lost Update Problem"
	
	private String name;

	@ElementCollection
	@CollectionTable(
			name="action_transition",
			joinColumns=@JoinColumn(name="transition_id")
	)
	@Column(name="action_id")
	// consider to change to Set<Integer> actionIds
	private Collection<Integer> associatedActionIDs;
	
	Transition() { }
	
	public Transition(Long id, String name, Collection<Integer> associatedActionIDs) {
		this.id = id;
		this.name = name;
		this.associatedActionIDs = new ArrayList<>(associatedActionIDs);
	}
	
	public Transition(String name) {
		this(null, name, Collections.emptyList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Collection<Integer> getAssociatedActionIDs() {
		return associatedActionIDs;
	}

	public void setAssociatedActionIDs(Collection<Integer> associatedActionIDs) {
		this.associatedActionIDs = associatedActionIDs;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Transition)) {
			return false;
		}
		
		return Objects.equals(id, ((Transition) obj).id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
