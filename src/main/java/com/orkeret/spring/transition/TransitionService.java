package com.orkeret.spring.transition;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.orkeret.spring.action.Action;
import com.orkeret.spring.action.ActionManager;
import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.CustomIllegalArgumentException;
import com.orkeret.spring.exception.ResourceNotFoundException;

@Service
public class TransitionService {

	@Autowired
	private TransitionDao transitionDao;
	
	@Autowired
	private ActionManager actionManager;
	
	public Optional<Transition> getTransition(Long id) {
		return transitionDao.findById(id);
	}
	
	public Iterable<Transition> getAllTransitions() {
		return transitionDao.findAll();
		
	}
	public Transition updateOrCreateTransition(Transition transition) {
		return transitionDao.save(transition);
	}

	public void deleteTransition(Long id) {
		try {
			transitionDao.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Transition with ID: " + id + " does not exist", e);
		} catch (DataIntegrityViolationException e) {
			throw new BadRequestException("Cannot delete transition [Transition ID: " + id + "]. The transition still participate in an active workflows.", e);
		}
	}
	
	// consider to change the signature to Set<Integer> actionIds
	public Transition associateActionsWithTransition(Collection<Integer> actionIds, Long transitionId) {
		Transition transition = transitionDao.findById(transitionId).orElseThrow(() -> new ResourceNotFoundException("Cannot find transition with ID: " + transitionId));
		validateActionIDs(actionIds);
		transition.setAssociatedActionIDs(actionIds);
		return transitionDao.save(transition);
	}

	/*
	 * at this point I can just check that the collection doesn't contain null values
	 * and the values are within the range of 1 to the number of "dynamically associable" actions
	 * I still wanted to future-proof it so I could introduce "deactivated" flag in the future more easily
	 * and this validation could be a soft spot
	 */
	/*
	 * consider to create a more informative exceptions, for now it would report about the first thing that will cause it to fail
	 * while there could be multiple reasons (e.g. a few actionsId there are null or referencing to a non-existing actions).
	 * The current approach still works but not ideal.
	 */
	private void validateActionIDs(Collection<Integer> actionIds) {
		for (Integer actionId : actionIds) {
			if (actionId == null) {
				throw new CustomIllegalArgumentException("Action ID cannot be null");
			}
			
			Action action = actionManager.getDynamicallyAssociableAction(actionId);
			if (action == null) { // future-proofing it
				throw new BadRequestException("Action with ID: " + actionId + " does not exist");
			}
			/*
			 * future enhancement could be:
			 * if (action.isDeactivated()) {
			 * 		throw an exception...
			 * }
			 */
		}
	}
}
