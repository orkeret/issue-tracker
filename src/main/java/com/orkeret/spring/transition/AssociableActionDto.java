package com.orkeret.spring.transition;

import com.orkeret.spring.action.Action;
import com.orkeret.spring.action.ActionDto;

public class AssociableActionDto extends ActionDto {

	private boolean associated;

	public AssociableActionDto(Action action, boolean associated) {
		super(action);
		this.associated = associated;
	}

	public boolean isAssociated() {
		return associated;
	}

	public void setAssociated(boolean associated) {
		this.associated = associated;
	}

}
