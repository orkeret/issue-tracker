package com.orkeret.spring.transition;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;


@Component
class TransitionResourceAssembler implements ResourceAssembler<Transition, Resource<TransitionDto>> {

	@Override
	public Resource<TransitionDto> toResource(Transition transition) {

		return new Resource<>(new TransitionDto(transition),
			linkTo(methodOn(TransitionController.class).getTransition(transition.getId())).withSelfRel(),
			linkTo(methodOn(TransitionController.class).getAllTransitions()).withRel("transitions"));
	}
}
