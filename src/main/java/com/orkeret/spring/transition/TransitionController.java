package com.orkeret.spring.transition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.action.ActionManager;
import com.orkeret.spring.common.BaseController;
import com.orkeret.spring.exception.ConflictException;
import com.orkeret.spring.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/transitions")
public class TransitionController extends BaseController<TransitionDto> {

	private static Logger logger = LoggerFactory.getLogger(TransitionController.class);
	
	@Autowired
	private TransitionService transitionService;
	
	@Autowired
	private TransitionResourceAssembler assembler;
	
	@Autowired
	private ActionManager actionManager;
	
	@GetMapping
	public Collection<Resource<TransitionDto>> getAllTransitions() {
		logger.info("request received to fetch all transitions");
		
		Collection<Resource<TransitionDto>> transitionDtoResources = new ArrayList<>();
		transitionService.getAllTransitions().forEach(transition -> transitionDtoResources.add(assembler.toResource(transition)));
		
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(transitionDtoResources, ... );

		return transitionDtoResources;
	}
	
	@PostMapping
	public ResponseEntity<Resource<TransitionDto>> createTransition(@RequestBody @Valid TransitionDto transitionDto) {
		logger.info("request to create new transition has been received");
		
		Transition transition = transitionService.updateOrCreateTransition(transitionDto.toDomainModel());
		
		return createResponseEntity(assembler.toResource(transition), transition.getVersion(), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Resource<TransitionDto>> getTransition(@PathVariable("id") Long id) {
		logger.info("request received to fetch transition with ID: " + id);
		validateID(id);
		Transition transition = transitionService.getTransition(id).orElseThrow(() -> new ResourceNotFoundException("Transition with ID: " + id + " does not exist"));

		return createResponseEntity(assembler.toResource(transition), transition.getVersion(), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Resource<TransitionDto>> updateTransition(WebRequest request, @RequestBody @Valid TransitionDto transitionDto, @PathVariable("id") Long id) {
		logger.info("request received to update transition with ID: " + id);
		validateID(id);
		// note that the transitionDto.id would get ignored - the ID from the pathParam would be the only ID that will be taken into account
		// consider creating a thinner version of transitionDto that doesn't contain an ID field
		
		return transitionService.getTransition(id).map(existingTransition -> { // transition exists!
			validateETag(request, existingTransition.getVersion());
			
			// need to make sure that all the fields (besides the ID and Version) are copied over from the dto to the non-dto object
			existingTransition.setName(transitionDto.getName());
			
			try {
				Transition updatedTransition = transitionService.updateOrCreateTransition(existingTransition);
				return createResponseEntity(assembler.toResource(updatedTransition), updatedTransition.getVersion(), HttpStatus.OK);
			} catch (OptimisticLockingFailureException e){
				/* 
				 * "Optimistic locking" is more scalable than "Pessimistic locking" when dealing with a
				 * highly concurrent environment. However pessimistic locking is a better solution for situations
				 * where the possibility of simultaneous updates to the same data by multiple sources is common
				 * 
				 * For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates.
				 * if proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:
				 * Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
				 * Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)
				 */
				throw new ConflictException("Failed updating transition ID: " + id + ". Another update taken place in the same time, please re-fetch the resource, apply the changes and try again", e);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("You are trying to edit a non-existing transition [Transition ID: " + id + "]"));
	}

	@DeleteMapping("/{id}")
	public void deleteTransition(@PathVariable("id") Long id) {
		logger.info("request received to delete transition with ID: " + id);
		validateID(id);
		// trying to delete a transition that has associated workflow transitions with fail and issue a 400 response with a message that says that
		transitionService.deleteTransition(id);
	}
	
	/*
	 * it assumes that the client-side would present all associable actions to the user (including both already associated as well as
	 * available to be associated, with a checkbox next to each action). The user then could select some, de-select some other actions.
	 * After doing so, the user would click "submit" and then the client-side would send the current state of all the associated action IDS to the server.
	 * If those are valid set of action IDs then the server would accept and set these associations as this transition's associated actions
	 */
	// Note: it is equivalent to a "set" method and not an "addAll" method
	@PostMapping("/{id}/actions")
	public Resource<TransitionDto> setActionsForTransition(@PathVariable("id") Long id, @RequestBody Set<Integer> actionIds) {
		logger.info("request received to associate transition ID: " + id + " with action IDs: " + Arrays.toString(actionIds.toArray()));
		validateID(id);
		
		Transition updatedTransition = transitionService.associateActionsWithTransition(actionIds, id);
		return assembler.toResource(updatedTransition);
	}
	
	// NOTE: it would return all associable actions but each one would have a flag to mark whether this action is already associated with the given transition
	@GetMapping("/{id}/associable-actions")
	public Collection<AssociableActionDto> getAssociableActionsForTransition(@PathVariable("id") Long id) {
		logger.info("request received to report all associable actions in respect to transition ID: " + id);
		validateID(id);
		
		Transition transition = transitionService.getTransition(id).orElseThrow(() -> new ResourceNotFoundException("Cannot find transition with ID: " + id));
		
		return actionManager.getAllDynamicallyAssociableActions().stream()
				.map(a -> new AssociableActionDto(a, transition.getAssociatedActionIDs().contains(a.getId())))
				.collect(Collectors.toList());
	}
}
