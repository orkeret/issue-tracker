package com.orkeret.spring.transition;

import org.springframework.data.repository.CrudRepository;

public interface TransitionDao extends CrudRepository<Transition, Long> {

}
