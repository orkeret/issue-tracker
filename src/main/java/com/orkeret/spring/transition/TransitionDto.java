package com.orkeret.spring.transition;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class TransitionDto {
	@Min(1)
	private Long id;
	
	@NotEmpty(message = "Name is a required field")
	private String name;

	private Collection<Integer> associatedActionIDs;
	
	public TransitionDto() { }
	
	public TransitionDto(Transition transition) {
		this.id = transition.getId();
		this.name = transition.getName();
		this.associatedActionIDs = new ArrayList<>(transition.getAssociatedActionIDs()); // not really need to create a new list..but what the heck
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Integer> getAssociatedActionIDs() {
		return associatedActionIDs;
	}

	public void setAssociatedActionIDs(Collection<Integer> associatedActionIDs) {
		this.associatedActionIDs = associatedActionIDs;
	}
	
	public Transition toDomainModel() {
		// we would ignore the id that the client sends us (in the payload, we would use the one from the path param) , we still keep this field to export the ID to the client
		return new Transition(name);
	}
	
	public String toString() {
		return "Transition [id: " + id + ", name: " + name + "]";
	}
}
