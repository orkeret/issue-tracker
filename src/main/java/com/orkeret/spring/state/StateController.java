package com.orkeret.spring.state;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.common.BaseController;
import com.orkeret.spring.exception.ConflictException;
import com.orkeret.spring.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/states")
public class StateController extends BaseController<StateDto> {

	private static Logger logger = LoggerFactory.getLogger(StateController.class);
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private StateResourceAssembler assembler;

	@GetMapping
	public Collection<Resource<StateDto>> getAllStates() {
		logger.info("request received to fetch all states");
		
		Collection<Resource<StateDto>> stateDtoResources = new ArrayList<>();
		stateService.getAllStates().forEach(state -> stateDtoResources.add(assembler.toResource(state)));
		
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(stateDtoResources, ... );

		return stateDtoResources;
	}
	
	@PostMapping
	public ResponseEntity<Resource<StateDto>> createState(@RequestBody @Valid StateDto stateDto) {
		logger.info("request to create new state has been received");
		
		State state = stateService.updateOrCreateState(stateDto.toDomainModel());
		
		return createResponseEntity(assembler.toResource(state), state.getVersion(),HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Resource<StateDto>> getState(@PathVariable("id") Long id) {
		logger.info("request received to fetch state with ID: " + id);
		validateID(id);
		
		State state = stateService.getState(id).orElseThrow(() -> new ResourceNotFoundException("State with ID: " + id + " does not exist"));

		return createResponseEntity(assembler.toResource(state), state.getVersion(), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Resource<StateDto>> updateState(WebRequest request, @RequestBody @Valid StateDto stateDto, @PathVariable("id") Long id) {
		logger.info("request received to update state with ID: " + id);
		validateID(id);
		// note that the stateDto.id would get ignored - the ID from the pathParam would be the only ID that will be taken into account
		// consider creating a thinner version of stateDto that doesn't contain an ID field
		
		return stateService.getState(id).map(existingState -> { // state exists!
			validateETag(request, existingState.getVersion());
			
			// need to make sure that all the fields (besides the ID and Version) are copied over from the dto to the non-dto object
			existingState.setName(stateDto.getName());
			
			try {
				State updatedState = stateService.updateOrCreateState(existingState);
				return createResponseEntity(assembler.toResource(updatedState), updatedState.getVersion(), HttpStatus.OK);
			} catch (OptimisticLockingFailureException e){
				/* 
				 * "Optimistic locking" is more scalable than "Pessimistic locking" when dealing with a
				 * highly concurrent environment. However pessimistic locking is a better solution for situations
				 * where the possibility of simultaneous updates to the same data by multiple sources is common
				 * 
				 * For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates.
				 * if proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:
				 * Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
				 * Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)
				 */
				throw new ConflictException("Failed updating state ID: " + id + ". Another update taken place in the same time, please re-fetch the resource, apply the changes and try again", e);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("You are trying to edit a non-existing state [State ID: " + id + "]"));
	}

	@DeleteMapping("/{id}")
	public void deleteState(@PathVariable("id") Long id) {
		logger.info("request received to delete state with ID: " + id);
		validateID(id);
		 // trying to delete a state that has associated workflow transitions with fail and issue a 400 response with a message that says that
		stateService.deleteState(id);
	}
}
