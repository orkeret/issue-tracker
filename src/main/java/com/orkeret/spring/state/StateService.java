package com.orkeret.spring.state;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.orkeret.spring.config.EntryState;
import com.orkeret.spring.config.EntryStateDao;
import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.ResourceNotFoundException;

@Service
public class StateService {
	private Long entryStateID; // NOTE: this field has lazy-init...so be careful if you access it directly
	
	@Autowired
	private StateDao stateDao;
	
	@Autowired
	private EntryStateDao entryStateDao;

	public Long getEntryStateID() {
		if (entryStateID == null) {
			List<EntryState> entryStates = entryStateDao.findEntryStates();
			if (entryStates.isEmpty()) {
				throw new IllegalStateException("EntryState instance is missing");
			}
			
			if (entryStates.size() > 1) {
				throw new IllegalStateException("There should be only 1 instance of EntryState but there are: " + entryStates.size() + " instances.");
			}
			
			// there is exactly one!
			entryStateID = entryStates.get(0).getEntryStateID();
		}
		
		return entryStateID;
	}

	public Optional<State> getState(Long id) {
		return stateDao.findById(id);
	}

	public Iterable<State> getAllStates() {
		return stateDao.findAll();
	}

	public State updateOrCreateState(State state) {
		return stateDao.save(state);
	}

	public void deleteState(Long id) {
		if (id.equals(getEntryStateID())) {
			// it's not mandatory to use the getEntryStateID() here as it would have been already initialised after the evaluation of the if-condition...but...still...
			throw new BadRequestException("You cannot delete ID: " + getEntryStateID() + ". This ID belong to the 'Entry State' which cannot be deleted.");
		}
		
		try {
			stateDao.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("State with ID: " + id + " does not exist", e);
		} catch (DataIntegrityViolationException e) {
			throw new BadRequestException("Cannot delete state [State ID: " + id + "]. The state still participate in an active workflows.", e);
		}
	}

}
