package com.orkeret.spring.state;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "state")
public class State {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version; // serves as its optimistic lock. will be part of handling the "Lost Update Problem"
	
	private String name;

	State() { }
	
	public State(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public State(String name) {
		this(null, name);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof State)) {
			return false;
		}
		
		return Objects.equals(id, ((State) obj).id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
