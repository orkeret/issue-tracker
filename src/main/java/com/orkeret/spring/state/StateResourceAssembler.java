package com.orkeret.spring.state;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;


@Component
class StateResourceAssembler implements ResourceAssembler<State, Resource<StateDto>> {

	@Override
	public Resource<StateDto> toResource(State state) {

		return new Resource<>(new StateDto(state),
			linkTo(methodOn(StateController.class).getState(state.getId())).withSelfRel(),
			linkTo(methodOn(StateController.class).getAllStates()).withRel("states"));
	}
}
