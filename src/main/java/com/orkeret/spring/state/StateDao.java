package com.orkeret.spring.state;

import org.springframework.data.repository.CrudRepository;

public interface StateDao extends CrudRepository<State, Long> {

}
