package com.orkeret.spring.state;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class StateDto {
	@Min(1)
	private Long id;
	
	@NotEmpty(message = "Name is a required field")
	private String name;
	
	public StateDto() { }

	public StateDto(State state) {
		this.id = state.getId();
		this.name = state.getName();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public State toDomainModel() {
		// we would ignore the id that the client sends us (in the payload, we would use the one from the path param) , we still keep this field to export the ID to the client
		return new State(name);
	}
	
	public String toString() {
		return "State [id: " + id + ", name: " + name + "]";
	}
}
