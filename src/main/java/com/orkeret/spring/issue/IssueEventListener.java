package com.orkeret.spring.issue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import com.orkeret.spring.action.ActionExecutor;

@Component
public class IssueEventListener {

	private static Logger logger = LoggerFactory.getLogger(IssueEventListener.class);

	@Autowired
	private ActionExecutor actionExecutor;

	@TransactionalEventListener
	public void processIssueStateChangedEvent(IssueStateChangedEvent event) {
		logger.trace("processIssueStateChangedEvent was invoked for Issue ID: " + event.getIssue().getId() + " and Transition ID: " + event.getTransition().getId());
		actionExecutor.executeActionsAfterTransition(event.getIssue(), event.getTransition());
	}
	
}