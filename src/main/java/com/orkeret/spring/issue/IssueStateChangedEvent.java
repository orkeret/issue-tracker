package com.orkeret.spring.issue;

import com.orkeret.spring.transition.Transition;

public class IssueStateChangedEvent {

	private final Issue issue;
	private final Transition transition;

	public IssueStateChangedEvent(Issue issue, Transition transition) {
		this.issue = issue;
		this.transition = transition;
	}
	
	public Issue getIssue() {
		return issue;
	}

	public Transition getTransition() {
		return transition;
	}

}