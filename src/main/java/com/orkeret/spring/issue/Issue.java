package com.orkeret.spring.issue;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.orkeret.spring.state.State;
import com.orkeret.spring.user.User;

@Entity
public class Issue {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;
    
	@Version
	private Long version; // serves as its optimistic lock. will be part of handling the "Lost Update Problem"
	
	/*
	 * should *not* have ON DELETE CASCADE - don't want to delete issues if their assignee has been removed
	 * a nice feature would be the ability to deactivate and (re)activate a user which is the preferred approach
	 * from keeping the history and auditing PoV
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="assigneeId")
	private User assignee;
	private String name;
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="stateId")
	private State state;

	Issue() { }

	public Issue(Long id, User assignee, String name, String description, State state) {
		this.id = id;
		this.assignee = assignee;
		this.name = name;
		this.description = description;
		this.state = state;
	}
	
	public Issue(User assignee, String name, String description) {
		this.assignee = assignee;
		this.name = name;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public User getAssignee() {
		return assignee;
	}

	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Issue)) {
			return false;
		}
		
		return Objects.equals(((Issue)obj).id, id);
	}
}