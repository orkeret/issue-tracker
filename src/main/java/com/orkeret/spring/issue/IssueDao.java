package com.orkeret.spring.issue;

import org.springframework.data.repository.CrudRepository;

public interface IssueDao extends CrudRepository<Issue, Long> {

}