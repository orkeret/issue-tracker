package com.orkeret.spring.issue;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.state.State;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.transition.TransitionService;
import com.orkeret.spring.workflow.WorkflowService;

//consider to add an inMemoryModel for optimising read OPs

@Service
public class IssueService {
	
	@Autowired
	private IssueDao issueDao;
	
	@Autowired
	private WorkflowService workflowService;
	
	@Autowired
	private TransitionService transitionService;
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	
	public Iterable<Issue> getAllIssues() {
		return issueDao.findAll();
	}
	
	@Transactional
	public Issue createIssue(Issue issue, Long transitionId) {
		try {
			Transition transition = transitionService.getTransition(transitionId).orElseThrow(() -> new ResourceNotFoundException("Transition doesn't exist [Transition ID: " + transitionId + "]"));
			State nextState = workflowService.getNextState(transitionId);
			issue.setState(nextState);
			Issue newIssue = issueDao.save(issue);
			
			applicationEventPublisher.publishEvent(new IssueStateChangedEvent(newIssue, transition));
			
			return newIssue;
		} catch (DataIntegrityViolationException e) { // checking just in case...
			throw new BadRequestException("Cannot create issue due to data integrity violation. Please check if the issue is associated with an exiting user.", e);
		} 
	}
	
	public Issue updateIssue(Issue issue) {
		try {
			return issueDao.save(issue);
		} catch (DataIntegrityViolationException e) { // checking just in case...
			throw new BadRequestException("Cannot update issue due to data integrity violation. Please check if the issue is associated with an exiting user.", e);
		} 
	}
	
	public Optional<Issue> getIssue(Long id) {
		return issueDao.findById(id);
	}
	
	/*
	 * easier and safe enough to implement it that way for now (could re-iterate on it later)
	 * by that point we know that it's a valid id, the question is if it belongs to an existing issue.
	 * If it's an existing issue then we would just delete it.
	 * If it's a non-existing issue we would throw an exception that would lead to a 404 not found.
	 * 
	 */
	public void deleteIssue(Long id) {
		try {
			issueDao.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Issue with ID: " + id + " does not exist", e);
		}
	}
	
	@Transactional
	public Issue makeTransition(Long issueId, Long transitionId) {
		Issue issue = issueDao.findById(issueId).orElseThrow(() -> new ResourceNotFoundException("Issue doesn't exist [Issue ID: " + issueId + "]"));
		Transition transition = transitionService.getTransition(transitionId).orElseThrow(() -> new ResourceNotFoundException("Transition doesn't exist [Transition ID: " + transitionId + "]"));
		State nextState = workflowService.getNextState(issue.getState().getId(), transitionId);
		issue.setState(nextState);
		Issue updatedIssue = issueDao.save(issue);
		
		applicationEventPublisher.publishEvent(new IssueStateChangedEvent(updatedIssue, transition));
		
		return updatedIssue;
	}
}
