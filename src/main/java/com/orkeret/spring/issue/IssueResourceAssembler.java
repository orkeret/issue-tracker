package com.orkeret.spring.issue;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class IssueResourceAssembler implements ResourceAssembler<Issue, Resource<IssueDto>> {

	@Override
	public Resource<IssueDto> toResource(Issue issue) {

		return new Resource<>(new IssueDto(issue),
			linkTo(methodOn(IssueController.class).getIssue(issue.getId())).withSelfRel(),
			linkTo(methodOn(IssueController.class).getAllIssues()).withRel("issues"));
	}
}
