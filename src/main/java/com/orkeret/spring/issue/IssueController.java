package com.orkeret.spring.issue;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.orkeret.spring.common.BaseController;
import com.orkeret.spring.exception.BadRequestException;
import com.orkeret.spring.exception.ConflictException;
import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.user.User;
import com.orkeret.spring.user.UserService;

@RestController
@RequestMapping("/issues")
public class IssueController extends BaseController<IssueDto> {
	// no need to use If-Match on the GET ("Read") operations (only interested to solve the "Lost Update Problem" at the moment)
	
	private static Logger logger = LoggerFactory.getLogger(IssueController.class);
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private IssueResourceAssembler assembler;
	
	@GetMapping
	public Collection<Resource<IssueDto>> getAllIssues() {
		logger.info("request received to fetch all issues");
		
		Collection<Resource<IssueDto>> issueDtoResources = new ArrayList<>();
		issueService.getAllIssues().forEach(issue -> issueDtoResources.add(assembler.toResource(issue)));
		// Future enhancement (for now it doesn't feel right as it would change the structure of the JSON and not just enhance it) - return new Resources<>(issueDtoResources, ... );
		
		return issueDtoResources;
	}
	
	/*
	 * the client-side would need to supply the initial tranisitonId. with the initial workflow steps configuration
	 * the only allowed transition from the "ENTRY STATE" is "CREATE ISSUE" but I wanted to future-proof it so
	 * the client-side would query for the initialWorkflowSteps to see which initial transitions
	 * are available and act accordingly.
	 */
	@PostMapping
	public ResponseEntity<Resource<IssueDto>> createIssue(@RequestBody @Valid IssueDto issueDto, @RequestParam("transitionId") Long transitionId) {
		logger.info("request to create new issue has been received");
		validateID(transitionId, "transitionId");
		
		User assignee = fetchAssignee(issueDto.getAssigneeId());
		Issue issue = issueDto.toDomainModel(assignee);
		Issue updatedIssue = issueService.createIssue(issue, transitionId);
		
		return createResponseEntity(assembler.toResource(updatedIssue), updatedIssue.getVersion() , HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Resource<IssueDto>> getIssue(@PathVariable("id") Long id) {
		logger.info("request received to fetch issue with ID: " + id);
		validateID(id);
		
		Issue issue = issueService.getIssue(id).orElseThrow(() -> new ResourceNotFoundException("Issue with ID: " + id + " does not exist"));

		return createResponseEntity(assembler.toResource(issue), issue.getVersion() , HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Resource<IssueDto>> updateIssue(WebRequest request, @RequestBody @Valid IssueDto issueDto, @PathVariable("id") Long id) {
		logger.info("request received to update issue with ID: " + id);
		validateID(id);
		// note that the IssueDto.id would get ignored - the ID from the pathParam would be the only ID that will be taken into account
		return issueService.getIssue(id).map(existingIssue -> { // issue already exists
			validateETag(request, existingIssue.getVersion());
			
			// need to make sure that all the fields (besides the ID and Version and the state) are copied over from the dto to the non-dto object
			User assignee = fetchAssignee(issueDto.getAssigneeId());
			existingIssue.setAssignee(assignee);
			existingIssue.setDescription(issueDto.getDescription());
			existingIssue.setName(issueDto.getName());

			try {
				Issue updatedIssue = issueService.updateIssue(existingIssue);
				return createResponseEntity(assembler.toResource(updatedIssue), updatedIssue.getVersion() , HttpStatus.OK);
			} catch (OptimisticLockingFailureException e){
				/* 
				 * "Optimistic locking" is more scalable than "Pessimistic locking" when dealing with a
				 * highly concurrent environment. However pessimistic locking is a better solution for situations
				 * where the possibility of simultaneous updates to the same data by multiple sources is common
				 * 
				 * For now I've decided to go with "Optimistic locking" as it's not common to have simultaneous updates.
				 * if proven otherwise could change approach to "Pessimistic lock". It could be done with explicitly setting:
				 * Repo CRUD "Write" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
				 * Repo CRUD "Read" methods to be annotated with @Lock(LockModeType.PESSIMISTIC_READ)
				 */
				throw new ConflictException("Failed updating issue ID: " + id + ". Another update taken place in the same time, please re-fetch the resource, apply the changes and try again", e);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("You are trying to edit a non-existing issue [Issue ID: " + id + "]"));
	}

	@DeleteMapping("/{id}")
	public void deleteIssue(@PathVariable("id") Long id) {
		// it doesn't really participate in the states and transitions workflow, but we could add executeActionsAfterTransition here if/when needed.
		logger.info("request received to delete issue with ID: " + id);
		validateID(id);
		
		issueService.deleteIssue(id);
	}
	
	@PostMapping("/{id}/transit")
	public ResponseEntity<Resource<IssueDto>> makeTransition(@PathVariable("id") Long id, @RequestParam("transitionId") Long transitionId) {
		logger.info("request received to make transition ID: " + transitionId + " to issue with ID: " + id);
		validateID(id);
		validateID(transitionId, "transitionId");
		
		Issue updatedIssue = issueService.makeTransition(id, transitionId);
		
		return createResponseEntity(assembler.toResource(updatedIssue), updatedIssue.getVersion() , HttpStatus.OK);
	}
	
	private User fetchAssignee(Long assigneeId) {
		User assignee = userService.getUser(assigneeId).orElseThrow(() -> new BadRequestException("The issue is referencing a non-existing user [User ID: " + assigneeId + "]"));
		return assignee;
	}
}
