package com.orkeret.spring.issue;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.orkeret.spring.user.User;

//consider creating an IssueRequest(Dto) and an IssueResponse(Dto), would be really similar, but still slightly different
public class IssueDto {

	@Min(1)
	private Long id;
	
	@Min(1) // if I'll create a dummy user I might will need to change it to Min(0)
	@NotNull(message = "Assignee ID is a required field") // currently, it's required that the client-side would provide an assigneeID on creation or a change of an issue 
	private Long assigneeId;
	
	@NotEmpty(message = "Name is a required field")
	private String name;
	private String description;
	private Long stateId;

	public IssueDto() { }

	public IssueDto(Issue issue) {
		this.id = issue.getId();
		this.assigneeId = issue.getAssignee().getId();
		this.name = issue.getName();
		this.description = issue.getDescription();
		this.stateId = issue.getState().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	
	public Issue toDomainModel(User assignee) {
		// we would ignore the state that the client sends us, we still keep this field to export the state to the client
		// we would ignore the id that the client sends us (in the payload, we would use the one from the path param) , we still keep this field to export the ID to the client
		Issue Issue = new Issue(assignee, name, description);
		
		return Issue;
	}
	
	public String toString() {
		return "Issue [id: " + id + ", name: " + name + ", assigneeId: " + assigneeId + ", description: " + description + ", stateId: " + stateId + "]";
	}
	
}