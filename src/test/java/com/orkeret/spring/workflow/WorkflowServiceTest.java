package com.orkeret.spring.workflow;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.orkeret.spring.exception.IllegalTransitionException;
import com.orkeret.spring.state.State;
import com.orkeret.spring.state.StateService;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.workflow.WorkflowService;
import com.orkeret.spring.workflow.WorkflowStep;
import com.orkeret.spring.workflow.WorkflowStepDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkflowServiceTest {
	
	private static final Long WRONG_SOURCE_STATE_ID = 100L;
	private static final Long WRONG_TRANSITION_ID = 200L;
	private WorkflowStep step;
	
	@Autowired
	private WorkflowService workflowService;
	
	@MockBean
	private WorkflowStepDao workflowStepDao;
	
	@MockBean
	private StateService stateService;
	
	@Before
	public void beforeTest() {
		final Long sourcStateId = 2L;
		final Long transitionId = 1L;
		final Long destStateId = 3L;
		
		State sourceState = new State(sourcStateId, "Some source state");
		Transition transition = new Transition(transitionId, "some transition", Collections.emptyList());
		State destState = new State(destStateId, "Some destination state");
		
		 step = new WorkflowStep("(state1, transition1) -> state2", sourceState, transition, destState);
		 
		 doReturn(step).when(workflowStepDao).findWorkflowStepBySourceStateAndTransition(sourceState.getId(), transition.getId());
	}
	
	@Test
	public void whenStateAndTransitionOfExistingStep_thenReturnsNextState() {		
		State calculatedDestState = workflowService.getNextState(step.getSourceState().getId(), step.getTransition().getId());
		
		verify(workflowStepDao).findWorkflowStepBySourceStateAndTransition(step.getSourceState().getId(), step.getTransition().getId());
		assertEquals(step.getDestinationState(), calculatedDestState);
	}
	
	@Test(expected=IllegalTransitionException.class)
	public void whenInvalidStateAndTransitionComboDueToWrongState_throwsException() {
		workflowService.getNextState(WRONG_SOURCE_STATE_ID , step.getTransition().getId());
		
		verify(workflowStepDao).findWorkflowStepBySourceStateAndTransition(WRONG_SOURCE_STATE_ID, step.getTransition().getId());
	}
	
	@Test(expected=IllegalTransitionException.class)
	public void whenInvalidStateAndTransitionComboDueToWrongTransition_throwsException() {
		workflowService.getNextState(step.getSourceState().getId() , WRONG_TRANSITION_ID);
		
		verify(workflowStepDao).findWorkflowStepBySourceStateAndTransition(step.getSourceState().getId(), WRONG_TRANSITION_ID);
	}
	
	@Test
	public void whenInitialStateAndTransitionOfValidStep_thenReturnsNextState() {
		doReturn(step.getSourceState().getId()).when(stateService).getEntryStateID();
		
		State nextState = workflowService.getNextState(step.getTransition().getId());
		
		verify(stateService).getEntryStateID();
		verify(workflowStepDao).findWorkflowStepBySourceStateAndTransition(step.getSourceState().getId(), step.getTransition().getId());
		assertEquals(step.getDestinationState(), nextState);
	}

}
