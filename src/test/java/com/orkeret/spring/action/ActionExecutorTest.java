package com.orkeret.spring.action;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Collection;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import com.orkeret.spring.issue.Issue;
import com.orkeret.spring.state.State;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.user.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActionExecutorTest {

	@Autowired
	private ActionExecutor actionExecutor;
	
	@MockBean
	private AsyncTaskExecutor asyncTaskExecutor;
	
	@MockBean
	private ActionManager actionManager;
	
	@Test
	public void whenTriggered_thenSubmitDefaultAndTransitionSpecificCallableActionsToTaskExecutor() {
		final Long issueID = 1L;
		final User assignee = null;
		final String issueName = "issue name";
		final String issueDesc = "issue description";
		final State state = null;
		final Issue issue = new Issue(issueID, assignee, issueName, issueDesc, state);
		
		final Long transitionID = 1L;
		final int associatedActionID = 1;
		final String transitionName = "some transition";
		final Collection<Integer> associatedActionIDs = Collections.singleton(associatedActionID);
		final Transition transition = new Transition(transitionID, transitionName, associatedActionIDs);
		
		final int action1ID = 2;
		final String action1Name = "my default action";
		final boolean action1IsDefaultAction = true;
		final Action action1 = new Action(action1ID, action1Name, action1IsDefaultAction, null);
		doReturn(Collections.singleton(action1)).when(actionManager).getAllDefaultActions();
		
		final String action2Name = "my associable action";
		final boolean action2IsDefaultAction = false;
		final Action action2 = new Action(associatedActionID, action2Name, action2IsDefaultAction, null);
		doReturn(action2).when(actionManager).getDynamicallyAssociableAction(associatedActionID);
		
		
		actionExecutor.executeActionsAfterTransition(issue, transition);
		
		verify(actionManager).getAllDefaultActions();
		verify(actionManager).getDynamicallyAssociableAction(associatedActionID);
		verify(asyncTaskExecutor).submit(new CallableAction(action1, issue));
		verify(asyncTaskExecutor).submit(new CallableAction(action2, issue));
		verifyNoMoreInteractions(asyncTaskExecutor, actionManager);
	}
	
}
