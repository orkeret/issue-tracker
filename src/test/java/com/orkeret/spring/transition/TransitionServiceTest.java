package com.orkeret.spring.transition;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransitionServiceTest {

	@Autowired
	private TransitionService transitionService;
	
	@MockBean
	private TransitionDao transitionDao;
	
	@Test
	public void whenAssociatingActionsWithTransition_thenPersistAssociations() {
		final Long transitionId = 1L;
		final String name = "CREATE ISSUE";
		final Transition transition = new Transition(transitionId, name, Collections.emptyList());
		final Collection<Integer> actionIDs = Collections.singleton(0);
		
		doReturn(Optional.of(transition)).when(transitionDao).findById(transitionId);
		doAnswer(i -> i.getArgument(0)).when(transitionDao).save(transition);
		
		Transition updatedTransition = transitionService.associateActionsWithTransition(actionIDs, transitionId);
		
		verify(transitionDao).findById(transitionId);
		verify(transitionDao).save(transition);
		assertEquals(actionIDs, updatedTransition.getAssociatedActionIDs());
	}
}
