package com.orkeret.spring.issue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import com.orkeret.spring.action.ActionExecutor;
import com.orkeret.spring.exception.ResourceNotFoundException;
import com.orkeret.spring.state.State;
import com.orkeret.spring.transition.Transition;
import com.orkeret.spring.transition.TransitionService;
import com.orkeret.spring.user.User;
import com.orkeret.spring.user.UserDao;
import com.orkeret.spring.workflow.WorkflowService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IssueServiceTest {
	
	@Autowired
	private IssueService issueService;
  
	@MockBean
	private IssueDao issueDao;
  
	@MockBean
	private UserDao userDao;
	
	@MockBean
	private ActionExecutor actionExecutor;
	
	@MockBean
	private WorkflowService workflowService;
	
	@MockBean
	private TransitionService transitionService;

	private Issue baseIssue;
	private Issue returnedIssue;
	private Transition transition;
	private State destState;
	
	@Before
	public void beforeTest() {
		final Long userID = 1L;
		final String email = "orkeret@gmail.com";
		final String name = "Or Keret";
		final User baseUser = new User(userID, email, name);
		
		final String issueName = "some meaningful name";
		final String description = "some random description";
		baseIssue = new Issue(baseUser, issueName, description);
		
		final State entryState = new State(2L, "entry state");
		returnedIssue = new Issue(1L, baseIssue.getAssignee(), baseIssue.getName(), baseIssue.getDescription(), entryState);
		
		/*
		 * the client-side would need to supply the initial tranisitonId, with the initial workflow steps configuration
		 * the only allowed transition from the "ENTRY STATE" is "CREATE ISSUE" but I wanted to future-proof it so
		 * the client-side would query for workflowService.getInitialWorkflowSteps() to see which initial transitions
		 * are available.
		 */
		final Long transitionId = 1L;
		final Long destStateId = 2L;
		transition = new Transition(transitionId, "some transition", Collections.emptyList());
		destState = new State(destStateId, "Some destination state");

		doReturn(returnedIssue).when(issueDao).save(baseIssue);
		doReturn(destState).when(workflowService).getNextState(entryState.getId(), transition.getId());
	}
	
	@Test
	public void whenCreateIssue_thenPersistIssue() {
		doReturn(Optional.of(transition)).when(transitionService).getTransition(transition.getId());
		
		Issue createdIssue = issueService.createIssue(baseIssue, transition.getId());
		
		verify(workflowService).getNextState(transition.getId());
		verify(actionExecutor).executeActionsAfterTransition(returnedIssue, transition);
		verify(issueDao).save(baseIssue);
		assertEquals(returnedIssue, createdIssue);
		assertEquals(destState, createdIssue.getState());
	}
	
	@Test
	public void whenUpdateIssue_thenPersistUpdatedIssue() {
		final Issue newIssue = new Issue(baseIssue.getId(), baseIssue.getAssignee(), "new name", "new desc", baseIssue.getState());
		
		issueService.updateIssue(newIssue);
		
		verify(issueDao).save(newIssue);
	}
	
	@Test
	public void whenDeleteIssue_thenDeleteFromRepository() {
		issueService.deleteIssue(returnedIssue.getId());
		
		verify(issueDao).deleteById(returnedIssue.getId());
	}
	
	@Test(expected=ResourceNotFoundException.class)
	public void whenDeleteNonExistingIssue_throwsException() {
		final Long nonExistingID = 100L;
		doThrow(new EmptyResultDataAccessException("No entities exists, expected at least 1", 1)).when(issueDao).deleteById(nonExistingID);
		
		issueService.deleteIssue(nonExistingID);
		
		verify(issueDao).deleteById(nonExistingID);
	}
	
	@Test
	public void whenStateChangeIsRequested_thenChangeStateAndPersist() {
		final Long sourcStateId = 10L;
		final Long transitionId = 10L;
		final Long destStateId = 11L;
		final State sourceState = new State(sourcStateId, "In Progress");
		final Transition transition = new Transition(transitionId, "some transition", Collections.emptyList());
		final State destState = new State(destStateId, "Some destination state");
		
		// will just create a copy of the "global" returnedIssue field and change it's state to the sourceState
		Issue issueInProgress = new Issue(
				returnedIssue.getId(), returnedIssue.getAssignee(), returnedIssue.getName(),
				returnedIssue.getDescription(), sourceState);
		
		Issue issueAfterStateChange = new Issue(
				issueInProgress.getId(), issueInProgress.getAssignee(), issueInProgress.getName(),
				issueInProgress.getDescription(), destState);
		
		doReturn(Optional.of(issueInProgress)).when(issueDao).findById(issueInProgress.getId());
		doReturn(issueAfterStateChange).when(issueDao).save(issueAfterStateChange);
		doReturn(Optional.of(transition)).when(transitionService).getTransition(transitionId);
		doReturn(destState).when(workflowService).getNextState(sourceState.getId(), transition.getId());
		
		Issue updatedIssue = issueService.makeTransition(issueInProgress.getId(), transition.getId());
		
		verify(workflowService).getNextState(sourceState.getId(), transition.getId());
		verify(actionExecutor).executeActionsAfterTransition(updatedIssue, transition);
		verify(issueDao).save(issueInProgress);
		assertEquals(destState, updatedIssue.getState());
		assertEquals(issueAfterStateChange, updatedIssue);
	}

}
