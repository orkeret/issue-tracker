package com.orkeret.spring.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	@Autowired
	private UserService userService;
  	
	@MockBean
  	private UserDao userDao;
	
	private User baseUser;
	
	@Before
	public void beforeTest() {
		final Long id = 1L;
		final String email = "orkeret@gmail.com";
		final String name = "Or Keret";
		baseUser = new User(id, email, name);
		baseUser.setVersion(0L);
	}
	
	@Test
	public void whenUpdateOrCreateUser_thenPersistUser() {
		doReturn(baseUser).when(userDao).save(baseUser);
		
		final User returnedUser = userService.updateOrCreateUser(baseUser);
		
		assertEquals(baseUser.getName(), returnedUser.getName());
		assertEquals(baseUser.getEmail(), returnedUser.getEmail());
		assertTrue(baseUser.getVersion() <= returnedUser.getVersion());
		assertEquals(baseUser, returnedUser);
		
		verify(userDao).save(baseUser);
	}

	@Test
	public void whenDeleteUser_thenDeleteFromRepository() {
		userService.deleteUser(baseUser.getId());
		
		verify(userDao).deleteById(baseUser.getId());
	}
	
	@Test
	public void whenGetUser_thenFindUserInRepository() {
		doReturn(Optional.of(baseUser)).when(userDao).findById(baseUser.getId());
		
		final Optional<User> persistedUser = userService.getUser(baseUser.getId());
		
		assertTrue(persistedUser.isPresent());
		assertEquals(baseUser, persistedUser.get());
		assertEquals(baseUser.getName(), persistedUser.get().getName());
		assertEquals(baseUser.getEmail(), persistedUser.get().getEmail());
		assertTrue(baseUser.getVersion() <= persistedUser.get().getVersion());
		
		verify(userDao).findById(baseUser.getId());
	}
	
	@Test
	public void whenGetAllUsers_thenFindAllUsersInRepository() {
		doReturn(Collections.singleton(baseUser)).when(userDao).findAll();
		
		final Iterable<User> persistedUsers = userService.getAllUsers();
		
		assertTrue(persistedUsers != null);
		Iterator<User> usersIt = persistedUsers.iterator();
		assertTrue(usersIt.hasNext());
		final User persistedUser = usersIt.next();
		
		
		assertEquals(baseUser, persistedUser);
		assertEquals(baseUser.getName(), persistedUser.getName());
		assertEquals(baseUser.getEmail(), persistedUser.getEmail());
		assertTrue(baseUser.getVersion() <= persistedUser.getVersion());
		assertFalse(usersIt.hasNext());
		
		verify(userDao).findAll();
	}
}
